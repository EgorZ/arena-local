Local arena - Kalah Engine
============

The engine for the Kalah competition at Confirmit student's olympiad.

This version of our Kalah engine has been set up for local use, for your own convenience. Note that this does *not* include the visualizer.

To run it you need to install Node.js v8.4.0 or higher ([Official site](https://nodejs.org))

### How to run
    cd [project folder]
    npm install
    node index.js [strategy1] [strategy2] strategy1Output.txt strategyOutput.txt arenaOutput.txt 
    
[strategy1] and [strategy2] - there may be a path to your strategies with the expansion of .cs or .js (ex. ../strategy.cs),  or a key word **test**

Next, you can specify three paths to the files that will contain the output of strategy 1, strategy 2 and arena, respectively.

### Configuration
The file config/default.js contains the configuration of the arena

* If you use C# to write strategies, you need to write MSBuild path to property **MsBuildPath**
* If you do not want the arena logs to be displayed in the console, then set the property **loggingToConsole: false**