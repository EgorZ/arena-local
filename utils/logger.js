const { createLogger, format, transports } = require('winston');
const { combine, timestamp, printf } = format;

const myFormat = printf(info => {
  return `(${info.timestamp}) ${info.level}: ${info.message}`;
});

function logger({console, filename}) {
  const loggerTransports = [];

  if (console) {
    loggerTransports.push(new transports.Console())
  }

  if (filename) {
    loggerTransports.push(new transports.File({filename}))
  }

  return createLogger({
    format: combine(
      timestamp(),
      myFormat
    ),
    transports: loggerTransports
  })
}

module.exports = logger;
