const KALAH_TURN_RESULT = {
  impossible: 'trImpossible',
  repeat: 'trRepeat',
  ok: 'trOk',
  over: 'trOver'
};

module.exports = {
  KALAH_TURN_RESULT
};