const {KALAH_TURN_RESULT} = require('./constants');

class Field {
  constructor() {
    this.cells = [
      6, 6, 6, 6, 6, 6, 0, 6, 6, 6, 6, 6, 6, 0
    ];
    this.isUpperTurn = false;
  }

  makeTurn(userTurnIndex) {

    if (!this._isTurnPossible(userTurnIndex)) {
      return KALAH_TURN_RESULT.impossible
    }

    const userKalahIndex = this._getKalahIndex(this.isUpperTurn);
    const opponentKalahIndex = this._getKalahIndex(!this.isUpperTurn);

    const newIndex = this._distributeStones(userTurnIndex, opponentKalahIndex);

    if (this.cells[newIndex] === 1) {
      this._takeOppositeCell(newIndex, userKalahIndex);
    }

    const repeat = (newIndex === userKalahIndex);
    if (!repeat) {
      this.isUpperTurn = !this.isUpperTurn;
    }

    if (this._checkTurnsExist()) {
      return KALAH_TURN_RESULT.over;
    }
    else {
      return repeat ? KALAH_TURN_RESULT.repeat : KALAH_TURN_RESULT.ok;
    }
  }

  _isTurnPossible(turnIndex) {
    if (this.cells[turnIndex] === undefined) {
      return false;
    }

    if (turnIndex === 6 || turnIndex === 13) {
      return false;
    }

    if (!this._isSelfCell(this.isUpperTurn, turnIndex)) {
      return false;
    }

    if (this.cells[turnIndex] === 0) {
      return false;
    }

    return true;
  }

  _distributeStones(userTurnIndex, opponentKalahIndex) {

    let stones = this.cells[userTurnIndex];
    this.cells[userTurnIndex] = 0;
    let currentIndex = userTurnIndex;

    while (stones > 0) {
      currentIndex++;
      if (currentIndex === opponentKalahIndex)
        currentIndex++;
      if (currentIndex === 14)
        currentIndex = 0;
      this.cells[currentIndex]++;
      stones--;
    }

    return currentIndex;
  }

  _checkTurnsExist() {
    let ourStones = 0;

    for (let i = 0; i <= this.cells.length; i++) {
      if (this._isSelfCell(this.isUpperTurn, i)) {
        ourStones += this.cells[i];
      }
    }

    return ourStones === 0;
  }

  _getKalahIndex(isUpper) {
    const lowerKalahIndex = 6;
    const upperKalahIndex = 13;
    return isUpper ? upperKalahIndex : lowerKalahIndex
  }

  _isSelfCell(upperTurn, cellIndex) {
    const upperUserCells = [7, 8, 9, 10, 11, 12];
    const lowerUserCells = [0, 1, 2, 3, 4, 5];

    const currentCells = upperTurn ? upperUserCells : lowerUserCells;
    return currentCells.includes(cellIndex);
  }

  _takeOppositeCell(turnIndex, kalahIndex) {

    if (!this._isSelfCell(this.isUpperTurn, turnIndex)) {
      return false
    }

    const oppositeCell = 12 - turnIndex;

    if (this.cells[oppositeCell] === 0) {
      return false
    }

    this.cells[kalahIndex] += this.cells[turnIndex] + this.cells[oppositeCell];

    this.cells[turnIndex] = 0;
    this.cells[oppositeCell] = 0;

    return true
  }

  getResult() {
    const {lowerKalahScore, upperKalahScore} = this._countScore();

    const result = {
      lower: lowerKalahScore,
      upper: upperKalahScore
    };

    if (lowerKalahScore === upperKalahScore) {
      result.winner = "none"
    } else if (lowerKalahScore < upperKalahScore) {
      result.winner = "upper"
    } else {
      result.winner = "lower"
    }

    return result;
  }

  _countScore() {
    let lowerKalahScore = this.cells[6];
    let upperKalahScore = this.cells[13];

    for (let i = 0; i < 6; i++) {
      lowerKalahScore += this.cells[i]
    }

    for (let i = 7; i < 13; i++) {
      upperKalahScore += this.cells[i]
    }

    return {
      lowerKalahScore, upperKalahScore
    }
  }
}

module.exports = Field;