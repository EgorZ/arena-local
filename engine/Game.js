const Field = require('./Field');
const {KALAH_TURN_RESULT} = require('./constants');

class Game{

  constructor(lower, upper, logger) {
    this.logger = logger;
    this.field = new Field();
    this.board = this.field.cells;

    this.logger.info('Game created');
    this.logger.info(`Initial board: ${this.board}`);

    this.lower = lower;
    this.upper = upper;
  }

  _getOpponent(player) {
    return this.lower === player ? this.upper : this.lower
  }

  step({turn, user}) {
    const opponent = this._getOpponent(user);

    const turnResult = this.field.makeTurn(turn);

    if (turnResult === KALAH_TURN_RESULT.impossible) {
      this.logger.info(`${turn} is incorrect input`);
      this.logger.info(`${opponent.name} win this game`);
      return {
        error: true
      };
    }

    this.logger.info(`User ${user.name} set ${turn} cell`);
    this.logger.info(`Now board: ${this.board}`);

    if (turnResult === KALAH_TURN_RESULT.ok) {
      return {
        error: false,
        nextPlayer: opponent,
        state: {board: this.board}
      }
    }

    if (turnResult === KALAH_TURN_RESULT.repeat) {
      this.logger.info(`User ${user.name} makes a move again`);
      return {
        error: false,
        nextPlayer: user,
        state: {board: this.board}
      };
    }

   return this._endGame({user, opponent});
  }

  _endGame({user, opponent}) {
    const result = this.field.getResult();
    this.logger.info(`User ${this.lower.name} have ${result.lower} score`);
    this.logger.info(`User ${this.upper.name} have ${result.upper} score`);


    const endedGame = {
      error: false,
      state: {board: this.board},
    };
    const winner = result.winner;

    if (winner === "none") {
      this.logger.info(`It's draw`);
      endedGame.result = "none";
    } else if (this[winner].name === user.name) {
      this.logger.info(`User ${user.name} win this game!`);
      endedGame.result = "win";
    } else {
      this.logger.info(`User ${opponent.name} win this game!`);
      endedGame.result = "lose";
    }

    this.logger.info("Game ended");

    return endedGame;
  }
}

module.exports = Game;
