const {spawn} = require('child_process');
const fs = require('fs-extra');

const langModel = require('./langModel');
const {compile} = require("../compilers/cs/compile");

class CsModel extends langModel{

  prepare() {
    const {data, name} = this;
    const compileResult = compile(name, data);

    if (compileResult.error) {
      this.error(`C# compilation error:\n${compileResult.error}`)
    }

    if (compileResult.result) {
      this.data = compileResult.result;
      return true
    }

    this.error(`Unknown C# compilation error`)
  }

  async start(initialState) {

    const {data, name} = this;
    const strategyPath = `${__dirname}\\${name}.exe`;

    fs.outputFileSync(strategyPath, data);

    this.player = spawn(strategyPath);

    this.player.stderr.on('data', (data) => {
      this.error(data.toString());
    });

    this.player.on('close', () => {
      fs.removeSync(strategyPath);
    });

    this.logger.info('The game start.');

    const jsonInitialState = JSON.stringify(initialState);
    this.logger.info(`Your initial state: ${jsonInitialState}`);

    return new Promise( resolve => {

      this.player.stdout.on('data', (data) => {
        if (data.toString() === "created") {
          resolve()
        }
      });

      this.player.stdin.write(`${jsonInitialState}\n`);
    });
  }

  async move(state) {
    const jsonState = JSON.stringify(state);
    this.logger.info(`You receive state: ${jsonState}`);

    this.player.stdin.write(`${jsonState}\n`);

    return new Promise( resolve => {

      this.player.stdout.on('data', turnCallback);

      const self = this;

      function turnCallback(data) {
        if (data.toString() === "created") {
          return;
        }

        const userMove = data.toString();
        self.logger.info(`Your output: ${userMove}`);
        self.player.stdout.removeListener('data', turnCallback);

        resolve(parseInt(userMove))
      }
    });
  }

  error(text) {
    this.logger.error(text);
    throw new Error(text);
  }

  endGame(result) {
    const messages = {
      none: 'It\'s draw',
      win: 'You win',
      lose: 'You lose'
    };

    const text = messages[result] || 'Result error';

    this.logger.info(text);
    this.player.kill();
  }
}

module.exports = CsModel;