const {NodeVM} = require('vm2');

const langModel = require('./langModel');

class JsModel extends langModel{

  prepare() {
    return true
  }

  async start(initialState) {
    const vm = new NodeVM();

    try {
      const PlayerClass = vm.run(this.data);
      this.player = new PlayerClass(initialState);
    } catch (e) {
      this.error(e);
    }

    this.logger.info('The game start.');
    this.logger.info(`Your initial state: ${JSON.stringify(initialState)}`);
  }

  async move(state) {
    this.logger.info(`You receive state: ${JSON.stringify(state)}`);

    let userMove;
    try {
      userMove = this.player.getTurn(state);
    } catch (e) {
      return this.error(e);
    }

    this.logger.info(`Your output: ${userMove}`);
    return userMove
  }

  error(error) {
    this.logger.error(`${error.name}: ${error.message}`);
    throw error;
  }

  endGame(result) {
    const messages = {
      none: 'It\'s draw',
      win: 'You win',
      lose: 'You lose'
    };

    const text = messages[result] || 'Result error';

    this.logger.info(text);
  }
}

module.exports = JsModel;
