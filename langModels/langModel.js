class LangModel {
  constructor({data, name, logger}) {
    this.data = data;
    this.name = name;
    this.logger = logger;
  }

  prepare() {
    throw new Error("You mustn't call method 'prepare' from abstract class")
  }

  async start() {
    throw new Error("You mustn't call method 'start' from abstract class")
  }

  async move() {
    throw new Error("You mustn't call method 'move' from abstract class")
  }

  endGame() {
    throw new Error("You mustn't call method 'endGame' from abstract class")
  }
}

module.exports = LangModel;