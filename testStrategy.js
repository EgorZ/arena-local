class Player {
  constructor(initialState) {
    this.isUpper = initialState.isUpper;

    if (this.isUpper) {
      this.myCells = [7, 8, 9, 10, 11, 12]
    } else {
      this.myCells = [0, 1, 2, 3, 4, 5]
    }
  }

  getTurn(state) {

    const board = state.board;

    let notEmpty = [];

    for (let i = 0; i < this.myCells.length; i++) {
      if (board[this.myCells[i]] > 0) {
        notEmpty.push(this.myCells[i])
      }
    }

    return notEmpty[0];
  }
}

module.exports = Player;
