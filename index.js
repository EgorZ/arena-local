const fs = require('fs');
const config = require('config');

const {JsModel, CsModel} = require("./langModels");
const createLogger = require('./utils/logger');
const Game = require('./engine/Game');

const langModels = {
  js: JsModel,
  cs: CsModel
};
const extensions = ["js", "cs"];

function getStrategy(name, filename, output) {

  let data, lang;
  if (filename === "test") {
    data = fs.readFileSync('./testStrategy.js');
    lang = "js";
  } else {
    data = fs.readFileSync(filename);
    lang = filename.split(".").pop();
  }

  if (!extensions.includes(lang)) {
    throwError(`File ${filename} has incorrect extension! It must be *.cs or *.js`);
  }

  const logger = createLogger({
    filename: output
  });

  const strategyLangModel = new langModels[lang]({data, name, logger});
  try {
    strategyLangModel.prepare();
  } catch(e) {
    throwError(`Problem with preparing ${filename}: \n${e.name}: ${e.message}`)
  }

  return strategyLangModel
}

function takeRandom(user, opponent) {
  const players = [user, opponent];
  const random = Math.floor(Math.random() * 2);

  const lower = players[random];
  const upper = players[1 - random];
  return {
    lower, upper
  }
}

function throwError(msg) {
  console.error(msg);
  process.exit(1);
}

async function runGame() {

  const [, , filename1, filename2, out1, out2, arenaOut] = process.argv;

  const strategy1 = getStrategy("first", filename1, out1);
  const strategy2 = getStrategy("second", filename2, out2);

  const {lower, upper} = takeRandom(strategy1, strategy2);

  try {
    await lower.start({isUpper: false});
  } catch(e) {
    throwError(`Problem with starting ${lower.name} \n${e.name}: ${e.message}`)
  }

  try {
    await upper.start({isUpper: true});
  } catch(e) {
    throwError(`Problem with starting: ${upper.name} \n${e.name}: ${e.message}`)
  }

  const mainLogger = createLogger({
    console: config.get("loggingToConsole"),
    filename: arenaOut
  });

  const game = new Game(lower, upper, mainLogger);

  let nextPlayer = lower;
  let state = {board: game.board};

  while (1) {

    let turn;
    try {
      turn = await nextPlayer.move(state);
    } catch(e) {
      throwError(`${e.name}: ${e.message}\n(Watch logs for more information)`)
    }

    const turnResult = game.step({turn, user: nextPlayer});

    const {error, result} = turnResult;

    const secondPlayer = nextPlayer === lower ? upper : lower;

    if (error) {
      nextPlayer.endGame("lose");
      secondPlayer.endGame("win");
      break;
    }

    if (!result) {
      nextPlayer = turnResult.nextPlayer;
      state = turnResult.state;
      continue;
    }

    if (result === "none") {
      nextPlayer.endGame("none");
      secondPlayer.endGame("none");
    } else if (result === "win") {
      nextPlayer.endGame("win");
      secondPlayer.endGame("lose");
    } else {
      nextPlayer.endGame("lose");
      secondPlayer.endGame("win");
    }

    break;
  }
}

runGame();
