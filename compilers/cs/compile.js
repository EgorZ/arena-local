const fs = require('fs-extra');
const config = require('config');
const {spawnSync} = require('child_process');

function compile(name, data) {
  const filename = `${__dirname}\\${name}`;

  fs.outputFileSync(`${filename}.cs`, data);

  const output = spawnSync(config.get("MsBuildPath"), ["Runner.csproj", `/property:Strategy=${name}.cs`,
    `/property:OutputName=${name}`, "/nologo", "/noconsolelogger", "/logger:Logger.dll"], {
    cwd: process.cwd() + "/compilers/cs"
  });

  const stdout = output.stdout.toString();

  fs.removeSync(`${filename}.cs`);
  fs.removeSync(`${filename}`);

  if (stdout !== "") {
    fs.removeSync(`${filename}.exe`);

    return {
      error: stdout
    };
  }

  const result = fs.readFileSync(`${filename}.exe`);
  fs.removeSync(`${filename}.exe`);

  return {
    result
  }

}

module.exports.compile = compile;