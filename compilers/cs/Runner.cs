﻿using System;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Security;
using System.Security.Policy;
using System.Security.Permissions;
using System.Reflection;
using System.Collections.Generic;

namespace Strategy
{
    class Program
    {
        static void Main(string[] args)
        {
            PermissionSet permissions = new PermissionSet(PermissionState.None);

            AppDomainSetup adSetup = new AppDomainSetup();
            adSetup.ApplicationBase = AppDomain.CurrentDomain.BaseDirectory;

            //Create a list of fully trusted assemblies
            Assembly[] asms = AppDomain.CurrentDomain.GetAssemblies();
            List<StrongName> sns = new List<StrongName>();
            for (int x = 0; x < asms.Length; x++)
            {
                StrongName sn = asms[x].Evidence.GetHostEvidence<StrongName>();
                if (sn != null && sns.Contains(sn) == false)
                    sns.Add(sn);
            }        

            AppDomain domain = AppDomain.CreateDomain("NewAppDomain", AppDomain.CurrentDomain.Evidence, adSetup, permissions, sns.ToArray());

            try
            {
                Console.Write("ready");

                string json = Console.ReadLine();
                MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(json));
                DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(InitialState));
                InitialState initialState = (InitialState)ser.ReadObject(ms);
                ms.Close();

                String asmName = Assembly.GetExecutingAssembly().ManifestModule.FullyQualifiedName;
                String typeName = typeof(Strategy.Player).FullName;

                var handle = Activator.CreateInstanceFrom(domain, asmName, typeName);
                var player = (Strategy.Player)handle.Unwrap();

                player.Init(initialState);
              
                Console.Write("created");

                string input = Console.ReadLine();
                while (input != "exit")
                {
                    ms = new MemoryStream(Encoding.UTF8.GetBytes(input));
                    ser = new DataContractJsonSerializer(typeof(State));
                    State state = (State)ser.ReadObject(ms);
                    ms.Close();
                    Console.Write(player.GetTurn(state));
                    input = Console.ReadLine();
                }
            }
            catch (SecurityException se)
            {
                Console.Error.Write(se.Message);
            }
            catch (Exception ex)
            {
                Console.Error.Write(ex.Message);
            }
        }
    }

    [Serializable]
    public class State
    {
        public byte[] board;
    }

    [Serializable]
    public class InitialState
    {
        public bool isUpper;
    }

    [Serializable]
    public partial class Player : System.MarshalByRefObject
    {

    }

}
